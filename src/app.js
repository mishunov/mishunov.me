// import style from "./css/base.css";

performance.mark('loading-start');

let currentView;
let baseView;

if (window.location.hash === '') {
  currentView= import(/* webpackChunkName: "frontpage" */ './js/frontpage');
} else {
  currentView = import(/* webpackChunkName: "section" */ './js/section');
}

currentView
.then(view => {
  performance.mark('main-view-here');
  // console.log('We have MAIN view');
  document.body.setAttribute('style', 'opacity: 1');
  view.sectionAnimation(window.location.hash.split('#')[1]);
  return import(/* webpackChunkName: "basejs" */ './js/base');
})
.then(view => {
  performance.mark('base-view-here');
  // console.log('We have BASE view');
  baseView = view;
  window.requestAnimationFrame(() => {
    if (window.location.hash === '') {
      return import(/* webpackChunkName: "section" */ './js/section');
    } else {
      return import(/* webpackChunkName: "frontpage" */ './js/frontpage');
    }
  });
})
.then(()=> {
  performance.mark('secondary-view-here');
  // console.log('We have SECONDARY view');
  baseView.intersectionObserverSetup();
  baseView.setNavigationScrolling();
  baseView.contactInfoToggleAnimationBootstrap();
})
.then(() => {
  performance.mark('request-analytics-view');
  // console.log('We dive into DNT view');
  if(window.requestIdleCallback) {
    window.requestIdleCallback(() => {
      import(/* webpackChunkName: "analytics" */ './js/analytics').then(view => {
        view.bootstrapAnalytics();
      });
    });
  } else {
    import(/* webpackChunkName: "analytics" */ './js/analytics').then(view => {
      view.bootstrapAnalytics();
    });
  }
});
