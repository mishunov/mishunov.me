import {Power4, TimelineLite} from 'gsap';

const template = import('../dnt.html');

function _thankUser() {
  const elemsToHide = document.querySelectorAll('#dnt-content > *:not(h2)');
  const thankYouTitle = document.querySelector('#dnt-content > h2');
  const thankYouTl = new TimelineLite({});
  thankYouTl
    .to(elemsToHide, .5, {autoAlpha: 0})
    .to(thankYouTitle, 2, {scale: '1.5', y: '+=100%', ease: Power4.easeOut}, '-=0.5')
    .to('#dnt-wrapper', .5, {autoAlpha: 0}, '+=1.5');
}

export function bootstrapDNT(_setUpAnalytics) {
  template.then( (html) => {
    const frag = document.createRange().createContextualFragment(html.default);
    document.getElementById('frontpage').appendChild(frag);

    const toggle = document.querySelector('#dnt-wrapper h1');
    const title = toggle.querySelector('.text-replace');
    const resetBtn = document.getElementById('reset');

    const enableAnalyticsBtn = document.getElementById('enable-analytics');
    const enableAnalytics = function() {
      enableAnalyticsBtn.removeEventListener('click', enableAnalytics);
      document.cookie = 'allow_analytics=true';
      _setUpAnalytics();
      _thankUser();
    };

    const tl = new TimelineLite({paused: true});

    tl
    .to(toggle, 0.5, {rotation:'0_cw'})
    .to(toggle, 0.5, {x: '0'}, '-=0.5')
    .to('#dnt-content', .5, {y: '0', ease: Power4.easeOut})
    .to('#dnt-content', .2, {opacity: 1}, '-=0.5')
    .to(resetBtn, 0.2, {x: '32px', ease: Power4.easeOut}, '-=0.2')
    .to(title, 0.5, {text:{value:'Do Not Track'}, ease:Power4.easeNone});


    enableAnalyticsBtn.addEventListener('click', enableAnalytics);
    toggle.addEventListener('click', (ev) => {
      if (ev.currentTarget !== resetBtn) {
        tl.play();
      }
    });
    resetBtn.addEventListener('click', (ev) => {
      ev.preventDefault();
      ev.stopPropagation();
      tl.reverse();
    });
  });
}
