/* eslint-disable no-unused-vars */
// import(/* webpackChunkName: 'frontpage-css' */'../css/frontpage.css');

// import {TimelineLite, Power2} from 'gsap';
import { Power2, TimelineLite, CSSPlugin } from './gsap-modules';
import { MorphSVGPlugin } from './gsap-bonus/MorphSVGPlugin.js';

import * as Animations from './animations';
const plugins = [MorphSVGPlugin, CSSPlugin];
/* eslint-enable no-unused-vars */

function _dropSplatter() {
  const tl = new TimelineLite({});
  const dropTop = document.getElementById('drop-top');
  const dropSelf = document.getElementById('drop-self-wrapper');
  const splatter = document.getElementById('drop-splatter');
  const bg = document.getElementById('drop-background');

  let scale = (() => {
    const bgSize = bg.getBoundingClientRect().width;
    return window.innerHeight > window.innerWidth? window.innerHeight/bgSize: window.innerWidth/bgSize;
  })();

  tl
  .set(dropTop, {autoAlpha: 1})
  .from(dropTop, 1, {scaleY:0.1, scaleX:4, transformOrigin:'top center', ease:Power2.easeNone})
  .set(dropSelf, {autoAlpha: 1})
  .fromTo(dropSelf, 1, {scale:0, y: '-=3vh', transformOrigin: 'top center'}, {scale: 1, y:'0', transformOrigin: 'top center', ease: Power2.easeNone}, '-=0.3')
  .to(dropSelf, 0.3, {scaleY: 1.2, transformOrigin: 'top center', ease: Power2.easeNone}, '-=0.7')
  .to(dropSelf, .1, {y: '+=55vh', ease: Power2.easeNone}, '-=0.1')
  .to(dropSelf, .1, {scaleY: '.2',  transformOrigin: 'bottom center', ease: Power2.easeNone}, '-=0.1')
  .to(dropTop, .1, {scaleY:0, transformOrigin:'top center', ease:Power2.easeNone}, '-=0.1')
  .set(splatter, {autoAlpha: 1})
  .set(dropSelf, {autoAlpha: 0})
  .from(splatter, .1, {scale: 0, transformOrigin: 'center center', ease: Power2.easeOut})
  .set(bg, {autoAlpha: 1})
  .to(bg, 1, {scale: scale, transformOrigin: 'center center', ease: Power2.easeNone}, '+=1')
  .from('#frontpage', 0.4, {backgroundColor: '#fff', ease:Power2.easeNone}, '-=0.4')
  .set(dropSelf, {autoAlpha: 0})
  .set(splatter, {autoAlpha: 0})
  .set(bg, {autoAlpha: 0});

  return tl;
}

function _fpSmileAnimation() {
  let tl = new TimelineLite({});
  tl
    .from('#smile', 1, {opacity: 0})
    .to('#mouth-end-contour-copy', 1, {morphSVG: '#mouth-end-contour', ease: Power2.easeOut}, '+=1')
    .to('#mouth-closed-contour', .3, {opacity: 0}, '-=1.5')
    .to('#mouth-closed-nasolabial-left', .7, {morphSVG: '#mouth-end-nasolabial-left', ease:  Power2.easeOut}, '-=1.5')
    .to('#mouth-closed-nasolabial-right', .7, {morphSVG: '#mouth-end-nasolabial-right', ease:  Power2.easeOut}, '-=1.5')
    .to('#mouth-closed', 0, {opacity: 0}, '-=1.2')
    .to('#mouth-end', 0, {opacity: 1}, '-=1.2');
  return tl;
}

export function sectionAnimation(short=false) {

  performance.mark('loading-end');
  performance.measure('loading', 'loading-start', 'loading-end');

  const el = document.getElementById('frontpage');

  let tl = new TimelineLite({
    onComplete: () => {
      document.body.setAttribute('ready', 'true');
      el.setAttribute('ready', 'true');
    }
  });

  if (!short) {
    tl.add(_dropSplatter());
  }

  document.body.dispatchEvent(new CustomEvent('section-ready'));

  tl
    .add(Animations.charAnimation('frontpage'))
    .add(_fpSmileAnimation(), '-=0.5')
    .add(Animations.stripesAnimation(), '-=0.5');

  return tl;
}
