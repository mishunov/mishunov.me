const path = require('path');
const webpack = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const MinifyPlugin = require('babel-minify-webpack-plugin');
const CssoWebpackPlugin = require('csso-webpack-plugin').default;
const CompressionPlugin = require('compression-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const postcssCustomProperties = require('postcss-custom-properties');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const pathToMainCss = require.resolve('../src/css/base.css');
const isModern = process.env.BROWSERSLIST_ENV === 'modern';
const modernAppEntry = [
  './src/js/polyfills.js',
  './src/app-http2.js'
];
const legacyAppEntry = [
  './src/js/polyfills.js',
  './src/app-http2.js'
];

const initialChunks = [
  'appEntry',
  'base-css',
  'gsap',
  'frontpage-css',
  'frontpage'
];

module.exports = {
  // entry: isModern? { modernAppEntry, pathToMainCss }: { legacyAppEntry, pathToMainCss },
  entry: {
    'appEntry': modernAppEntry,
    'base-css': ['./src/css/base.css'],
    'gsap': ['./src/js/gsap-modules.js'],
    'frontpage-css': ['./src/css/frontpage.css'],
  },
  mode: 'production',
  output: {
    filename: '[name]-bundle.js',
    chunkFilename: '[id]-[contenthash].bundle.js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/'
  },
  optimization: {
    splitChunks: {
      chunks: 'all'
    },
    // splitChunks: {
    //   chunks (chunk) {
    //     return initialChunks.indexOf(chunk.name) !== -1;
    //   }
    // },
    minimizer: [
      new MinifyPlugin(),
      new CssoWebpackPlugin({
        'comments': false
      })
    ]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader'
          }
        ]
      },
      {
        test: /\.css$/,
        use: [MiniCSSExtractPlugin.loader, 'css-loader']
      },
      {
        test: /\.(jpg|png|svg|webp)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {minimize: true}
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new CleanWebpackPlugin(['../dist'], {
      allowExternal: true
    }),
    new HTMLWebpackPlugin({
      template: './src/index.html'
    }),
    new MiniCSSExtractPlugin({
      filename: '[name]-[contenthash].css',
      chunkFilename: '[name]-[contenthash].css'
    }),
    new FaviconsWebpackPlugin({
      logo: './src/favicon.svg',
      icons: {
        android: false,
        appleIcon: false,
        appleStartup: false,
        coast: true,
        favicons: true,
        firefox: true,
        opengraph: false,
        twitter: true,
        yandex: true,
        windows: false
      }
    }),
    new PreloadWebpackPlugin({
      rel: 'preload',
      include: [
        // 'gsap',
        // 'appEntry',
        // 'base-css',
        // 'frontpage-css',
        // 'frontpage',
        // 'vendors~appEntry',
        // 'vendors~frontpage~gsap',
        // 'exit-transitions-css',
        // 'section-css'
        'gsap',
        'appEntry',
        'base-css',
        'frontpage-css',
        'frontpage',
        'vendors~appEntry',
        'vendors~basejs~frontpage~gsap~section',
        'basejs~frontpage',
        'exit-transitions-css',
        'section-css'
      ]
    }),
    new webpack.NamedModulesPlugin(),
    new CopyWebpackPlugin([
      {
        from: './src/img/social-preview.png',
        to: path.resolve(__dirname, '../dist/')
      },
      {
        from: './.well-known',
        to: path.resolve(__dirname, '../dist/.well-known')
      },
    ]),
    new CompressionPlugin({
      algorithm: 'gzip',
      exclude: /_headers/
    }),
    new BrotliPlugin(),
    new ManifestPlugin()
  ]
};
