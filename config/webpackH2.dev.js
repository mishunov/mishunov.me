const path = require('path');
const webpack = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const postcssCustomProperties = require('postcss-custom-properties');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin;
const CopyWebpackPlugin = require('copy-webpack-plugin');

const pathToMainCss = require.resolve('../src/css/base.css');
const isModern = process.env.BROWSERSLIST_ENV === 'modern';
const modernAppEntry = [
  './src/js/polyfills.js',
  '@babel/runtime/regenerator',
  '@babel/register',
  'webpack-hot-middleware/client?reload=true',
  './src/app-http2.js'
];
const legacyAppEntry = [
  './src/js/polyfills.js',
  '@babel/runtime/regenerator',
  '@babel/register',
  'webpack-hot-middleware/client?reload=true',
  './src/app-http2.js'
];

const initialChunks = [
  'appEntry',
  'base-css',
  'gsap',
  'frontpage-css',
  'frontpage'
];

module.exports = {
  // entry: isModern? { modernAppEntry, pathToMainCss }: { legacyAppEntry, pathToMainCss },
  entry: {
    'appEntry': modernAppEntry,
    'base-css': ['./src/css/base.css'],
    'gsap': ['./src/js/gsap-modules.js'],
    'frontpage-css': ['./src/css/frontpage.css']
  },
  mode: 'development',
  output: {
    filename: '[name]-bundle.js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/'
  },
  devServer: {
    contentBase: 'dist',
    overlay: true,
    stats: {
      colors: true
    }
  },
  optimization: {
    // splitChunks: {
    //   chunks (chunk) {
    //     return initialChunks.indexOf(chunk.name) !== -1;
    //   }
    // }
    splitChunks: {
      chunks: 'all'
    }
  },
  // optimization: {
  //   splitChunks: {
  //     chunks: "all"
  //   }
  // },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader'
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          MiniCSSExtractPlugin.loader,
          { loader: 'css-loader', options: { importLoaders: 1 } }
        ]
      },
      {
        test: /\.(jpg|png|svg|webp)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader'
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      }
    }),
    new webpack.HotModuleReplacementPlugin(),
    new HTMLWebpackPlugin({
      template: './src/index.html'
    }),
    new PreloadWebpackPlugin({
      rel: 'preload',
      include: [
        'gsap',
        'appEntry',
        'base-css',
        'frontpage-css',
        'frontpage',
        'vendors~appEntry',
        'vendors~basejs~frontpage~gsap~section',
        'basejs~frontpage',
        'exit-transitions-css',
        'section-css'
      ]
    }),
    new CopyWebpackPlugin([
      {
        from: './src/css/noscript.css',
        to: path.resolve(__dirname, '../dist/')
      }
    ]),
    new MiniCSSExtractPlugin({
      filename: '[name].css'
    }),
    // new webpack.NamedModulesPlugin(),
    // new BundleAnalyzerPlugin({
    //   generateStatsFile: true
    // }),
  ]
};
