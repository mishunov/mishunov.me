const path = require('path');
const webpack = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const MinifyPlugin = require('babel-minify-webpack-plugin');
const CssoWebpackPlugin = require('csso-webpack-plugin').default;
const CompressionPlugin = require('compression-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const postcssCustomProperties = require('postcss-custom-properties');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');

const pathToMainCss = require.resolve('../src/css/base.css');
// const isModern = process.env.BROWSERSLIST_ENV === 'modern';
// const modernAppEntry = [
//   './src/app.js'
// ];
// const legacyAppEntry = [
//   './src/js/polyfills.js',
//   './src/app.js'
// ];

module.exports = {
  // entry: isModern? { modernAppEntry, pathToMainCss }: { legacyAppEntry, pathToMainCss },
  entry: {
    app: [
      './src/js/polyfills.js',
      './src/app.js'
    ],
    pathToMainCss
  },
  mode: 'production',
  output: {
    filename: '[name]-bundle.js',
    chunkFilename: '[id]-[contenthash].bundle.js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/'
  },
  optimization: {
    splitChunks: {
      chunks: "all"
    },
    minimizer: [
      new MinifyPlugin(),
      new CssoWebpackPlugin({
        'comments': false
      })
    ]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader'
          }
        ]
      },
      {
        test: /\.css$/,
        exclude: pathToMainCss,
        use: [MiniCSSExtractPlugin.loader, 'css-loader']
      },
      {
        test: pathToMainCss,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [
                postcssCustomProperties()
              ]
            }
          }
        ]
      },
      {
        test: /\.(jpg|png|svg|webp)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {minimize: true}
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new CleanWebpackPlugin(['../dist'], {
      allowExternal: true
    }),
    new HTMLWebpackPlugin({
      template: './src/index.html'
    }),
    new MiniCSSExtractPlugin({
      filename: '[name]-[contenthash].css',
      chunkFilename: '[name]-[contenthash].css'
    }),
    new FaviconsWebpackPlugin({
      logo: './src/favicon.svg',
      icons: {
        android: false,
        appleIcon: false,
        appleStartup: false,
        coast: true,
        favicons: true,
        firefox: true,
        opengraph: false,
        twitter: true,
        yandex: true,
        windows: false
      }
    }),
    new PreloadWebpackPlugin({
      rel: 'preload',
      include: [
        'pathToMainCss',
        'frontpage',
        'frontpage-css',
        'section-css',
        'app',
        'exit-transitions-css',],
    }),
    new CompressionPlugin({
      algorithm: 'gzip'
    }),
    new BrotliPlugin(),
    new ManifestPlugin()
  ]
};
